package com.gabelwright.teacheraid;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.gabelwright.teacheraid.items.Feedback;

import java.util.HashMap;
import java.util.List;

public class PastFeedbackListAdaptor extends BaseExpandableListAdapter {
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<Feedback>> _listDataChild;

    public PastFeedbackListAdaptor(Context context, List<String> listDataHeader, HashMap<String, List<Feedback>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return this._listDataChild.get(this._listDataHeader.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.past_feedback_list_row, null);
        }

        TextView groupName = convertView.findViewById(R.id.past_feedback_date);
        TextView groupSize = convertView.findViewById(R.id.past_feedback_size);

        Long created = Long.parseLong(this._listDataHeader.get(groupPosition));
        String name = Utils.convertToDate(created);

        groupName.setText(name);
        groupSize.setText("Feedback: " + this.getChildrenCount(groupPosition));

        int positive = 0;
        int negative = 0;
        for(int i=0; i<this.getChildrenCount(groupPosition);i++){
            Feedback pf = (Feedback) this.getChild(groupPosition, i);
            if(pf.isPositive())
                positive++;
            else
                negative++;
        }

        FeedbackSummaryBarDrawable bar = new FeedbackSummaryBarDrawable(_context, positive, negative);

        groupSize.setBackground(bar);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final Feedback childFeedback = (Feedback) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.feedback_list_row, null);
        }

        CardView card = convertView.findViewById(R.id.feedback_row_card);
        if(childFeedback.isPositive())
            card.setCardBackgroundColor(_context.getResources().getColor(R.color.positive_feedback_selected));
        else
            card.setCardBackgroundColor(_context.getResources().getColor(R.color.negative_feedback_selected));

        TextView txtListChild = convertView.findViewById(R.id.feedback_row_text);

        txtListChild.setText(childFeedback.getName());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

    static void log(String s){
        Log.i("mgw", s);
    }

}


