package com.gabelwright.teacheraid;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.view.View;

public class FeedbackSummaryBarDrawable extends Drawable {
    private final Paint redPaint;
    private final Paint greenPaint;
    private int positive;
    private int negative;

    public FeedbackSummaryBarDrawable(Context context, int positive, int negative) {
        // Set up color and text size
        redPaint = new Paint();
        redPaint.setColor(context.getResources().getColor(R.color.negative_feedback_selected));
        greenPaint = new Paint();
        greenPaint.setColor(context.getResources().getColor(R.color.positive_feedback_selected));
        this.positive = positive;
        this.negative = negative;
    }

    @Override
    public void draw(Canvas canvas) {
        // Get the drawable's bounds
        int width = getBounds().width();
        int height = getBounds().height();

        float barSize = (float) width / (float) (positive + negative);

        canvas.drawRect(0, 0, barSize * positive, height, greenPaint);
        canvas.drawRect(barSize * positive, 0, width, height, redPaint);
    }

    @Override
    public void setAlpha(int alpha) {
        // This method is required
    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {
        // This method is required
    }

    @Override
    public int getOpacity() {
        // Must be PixelFormat.UNKNOWN, TRANSLUCENT, TRANSPARENT, or OPAQUE
        return PixelFormat.OPAQUE;
    }
}

