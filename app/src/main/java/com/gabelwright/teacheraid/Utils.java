package com.gabelwright.teacheraid;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Utils {
    private static final int KEY_LENGTH = 6;

    public static String genRandomKey(){
        int leftLimit = 65;
        int rightLimit = 90;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(KEY_LENGTH);
        for (int i = 0; i < KEY_LENGTH; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    public static String parseTeacherId(String id){
        try{
            return id.substring(0, 6);
        }
        catch (Exception e){
            return null;
        }
    }

    public static String parseCourseId(String id){
        try{
            return id.substring(0, 12);
        }
        catch (Exception e){
            return null;
        }
    }

    public static String convertToDate(long val){
        Date date = new Date(val);
        SimpleDateFormat df2 = new SimpleDateFormat("LLLL dd");
        String dateText = df2.format(date);
        return dateText;
    }

    public static String getUserType(Context context){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(context.getString(R.string.USER_TYPE_KEY), "not found");
    }

    public static String getUserId(Context context, String type){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(type, "");
    }

}
