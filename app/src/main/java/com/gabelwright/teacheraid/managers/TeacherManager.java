package com.gabelwright.teacheraid.managers;

import com.gabelwright.teacheraid.Utils;
import com.gabelwright.teacheraid.items.Item;
import com.gabelwright.teacheraid.managers.Manager;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.Date;

public class TeacherManager extends Manager {

    public TeacherManager() {
        super(new ArrayList<Item>(), "");
    }

    public String createTeacher(String name){
        String key = Utils.genRandomKey();

        DatabaseReference ref = getRef(TEACHER_REF, key);
        ref.child("name").setValue(name);
        ref.child("created").setValue(new Date().getTime());

        return key;
    }

}
