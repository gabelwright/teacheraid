package com.gabelwright.teacheraid.managers;

import androidx.annotation.NonNull;

import com.gabelwright.teacheraid.items.Chat;
import com.gabelwright.teacheraid.items.Item;
import com.gabelwright.teacheraid.managers.Manager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ChatManager extends Manager {
    public ChatManager(String key){
        super(new ArrayList<Item>(), key);

        DatabaseReference ref = getRef(CHAT_REF, key);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                clearList();
                for(DataSnapshot data : dataSnapshot.getChildren()){
                    Chat c = data.getValue(Chat.class);
                    addItem(c);
                }
                if (getListener() != null) {
                    getListener().onChangeHappened();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void saveMessage(String message, String key){
        Chat chat = new Chat(message, key);

        DatabaseReference ref = getRef(CHAT_REF, getKey());
        String parentKey = ref.push().getKey();
        ref.child(parentKey).setValue(chat);
    }

}
