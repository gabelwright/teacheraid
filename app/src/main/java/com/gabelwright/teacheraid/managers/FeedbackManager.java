package com.gabelwright.teacheraid.managers;

import android.util.SparseBooleanArray;

import com.gabelwright.teacheraid.Utils;
import com.gabelwright.teacheraid.items.Feedback;
import com.gabelwright.teacheraid.items.Item;
import com.gabelwright.teacheraid.managers.Manager;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.Date;

public class FeedbackManager extends Manager {

    private String[] positiveFeedbackNames = {
            "Arrived on time to class",
            "Arrived prepared for class",
            "Completed all homework",
            "Completed all classwork",
            "Participated in class",
            "Worked well with others"
    };

    private String[] negativeFeedbackNames = {
            "Arrived late to class",
            "Was not prepared for class",
            "Did not complete homework",
            "Did not complete classwork",
            "Did not participate in class",
            "Did not cooperate with others",
            "Slept during class"
    };

    public FeedbackManager(String teacher_id){
        super(new ArrayList<Item>(), teacher_id);
    }

    public void populateWithNewFeedback(){
        for(String value : positiveFeedbackNames){
            Feedback feedback = new Feedback(value, "", true);
            addItem(feedback);
        }
        for(String value : negativeFeedbackNames){
            Feedback feedback = new Feedback(value, "", false);
            addItem(feedback);
        }
    }

    public void leaveFeedback(SparseBooleanArray sbArray, String studentId){
        DatabaseReference ref = getRef(FEEDBACK_REF, studentId);
        Long timestamp = new Date().getTime();
        ref = ref.child(timestamp.toString());

        for(int i=0;i<sbArray.size();i++){
            int key = sbArray.keyAt(i);
            if(sbArray.get(key)){
                String dbKey = ref.push().getKey();
                Feedback feedback = (Feedback) getItem(key);
                feedback.setKey(dbKey);
                ref.child(dbKey).setValue(feedback);
            }
        }
        updateLastFeedback(studentId);
    }

    public void updateLastFeedback(String studentId){
        DatabaseReference ref = getRef(STUDENT_REF, Utils.parseCourseId(studentId));
        Long timestamp = new Date().getTime();
        ref.child(studentId).child("updated").setValue(timestamp);
    }


}
