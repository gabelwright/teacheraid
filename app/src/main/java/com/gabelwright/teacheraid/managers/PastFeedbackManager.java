package com.gabelwright.teacheraid.managers;

import androidx.annotation.NonNull;

import com.gabelwright.teacheraid.Utils;
import com.gabelwright.teacheraid.items.Feedback;
import com.gabelwright.teacheraid.managers.BaseManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class PastFeedbackManager extends BaseManager {

    private HashMap<String, List<Feedback>> childFeedback;
    private List<String> headers;
    private String student_id;

    public PastFeedbackManager(String student_id) {

        childFeedback = new HashMap<>();
        headers = new ArrayList<>();
        this.student_id = student_id;

        DatabaseReference ref = getRef("feedback", student_id);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                headers.clear();
                childFeedback.clear();
                for(DataSnapshot data : dataSnapshot.getChildren()){
                    String name = data.getKey();
                    headers.add(name);
                    List<Feedback> dateFeedbackList = new ArrayList<>();
                    for(DataSnapshot f : data.getChildren()){
                        Feedback feedback = f.getValue(Feedback.class);
                        assert feedback != null;
                        dateFeedbackList.add(feedback);
                    }
                    childFeedback.put(name, dateFeedbackList);
                }
                Collections.reverse(headers);
                if (getListener() != null) {
                    getListener().onChangeHappened();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void deleteGroup(int position){
        DatabaseReference ref = getRef(FEEDBACK_REF, student_id);
        ref.child(getHeader(position)).removeValue();
        updateLastFeedback();
    }

    public void updateLastFeedback(){
        DatabaseReference ref = getRef(STUDENT_REF, Utils.parseCourseId(student_id));
        ref = ref.child(student_id).child("updated");
        if(headers.size() > 1)
            ref.setValue(Long.parseLong(headers.get(1)));
        else
            ref.removeValue();
    }

    public boolean isTodaysFeedback(int position){
        long now = new Date().getTime();
        long feedbackDate = Long.parseLong(getHeader(position));
        if(Utils.convertToDate(now).equals(Utils.convertToDate(feedbackDate)))
            return true;
        return false;
    }

    public HashMap<String, List<Feedback>> getChildFeedback() {
        return childFeedback;
    }

    public String getHeader(int position){
        return headers.get(position);
    }

    public String getHeaderName(int position){
        Long created = Long.parseLong(getHeader(position));
        String name = Utils.convertToDate(created);
        return name;
    }

    public List<String> getHeaders() {
        return headers;
    }

    public String getStudent_id() {
        return student_id;
    }

}
