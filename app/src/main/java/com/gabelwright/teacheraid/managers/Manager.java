package com.gabelwright.teacheraid.managers;

import android.util.Log;

import com.gabelwright.teacheraid.items.Item;
import com.gabelwright.teacheraid.items.SortByAlpha;
import com.gabelwright.teacheraid.managers.BaseManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

abstract public class Manager extends BaseManager {
    private List<Item> items;
    private String key;
    private ChangeListener listener;

    final String COURSE_REF = "courses";
    final String STUDENT_REF = "students";
    final String TEACHER_REF = "teachers";
    final String FEEDBACK_REF = "feedback";
    final String CHAT_REF = "chats";

    Manager(ArrayList<Item> list, String _key){
        items = list;
        this.key = _key;
    }

    String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    void clearList(){
        items.clear();
    }

    void addItem(Item item){
        items.add(item);
    }

    public List<Item> getItems(){
        return items;
    }

    Item getItem(int position){
        return items.get(position);
    }

    /** Sorts list by alpha */
    void sortByAlpha(){
        Collections.sort(getItems(), new SortByAlpha());
        if (getListener() != null) {
            getListener().onChangeHappened();
        }
    }

    public String toString(){
        return "manager item " + getKey();
    }

    static void log(String s){
        Log.i("mgw", s);
    }
}
