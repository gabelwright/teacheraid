package com.gabelwright.teacheraid.managers;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gabelwright.teacheraid.Utils;
import com.gabelwright.teacheraid.items.Course;
import com.gabelwright.teacheraid.items.Item;
import com.gabelwright.teacheraid.items.Student;
import com.gabelwright.teacheraid.managers.Manager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class StudentManager extends Manager {

    public StudentManager(String course_id){
        super(new ArrayList<Item>(), course_id);

        DatabaseReference ref = getRef(STUDENT_REF, course_id);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                clearList();
                for(DataSnapshot data : dataSnapshot.getChildren()){
                    Student s = data.getValue(Student.class);
                    addItem(s);
                }
                sortByAlpha();
                if (getListener() != null) {
                    getListener().onChangeHappened();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public Student getStudent(int position){
        return (Student) getItem(position);
    }

    public void createStudent(String name){
        DatabaseReference ref = getRef(STUDENT_REF, getKey());
        String student_id = getKey() + Utils.genRandomKey();
        Student s = new Student(name, student_id, 0);
        ref.child(student_id).setValue(s);
        updateClassSize(1);
    }

    public void updateName(int position, String name){
        Student student = (Student) getItem(position);
        DatabaseReference ref = getRef(STUDENT_REF, Utils.parseCourseId(student.getKey()));
        ref.child(student.getKey()).child("name").setValue(name);
    }

    public void deleteStudent(int position){
        Student student = (Student) getItem(position);
        DatabaseReference ref = getRef(STUDENT_REF, Utils.parseCourseId(student.getKey()));
        ref.child(student.getKey()).removeValue();
        updateClassSize(-1);
    }

    void updateClassSize(final int i){
        DatabaseReference ref = getRef(COURSE_REF, Utils.parseTeacherId(getKey()));
        ref = ref.child(getKey());
        ref.runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                Course c = mutableData.getValue(Course.class);
                if(c != null){
                    int size = c.getSize() + i;
                    c.setSize(size);
                    mutableData.setValue(c);
                }
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {
                if (getListener() != null) {
                    getListener().onChangeHappened();
                }
            }
        });
    }

}
