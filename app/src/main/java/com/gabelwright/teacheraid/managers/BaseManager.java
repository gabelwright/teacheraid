package com.gabelwright.teacheraid.managers;

import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

abstract public class BaseManager {
    private ChangeListener listener;

    final String COURSE_REF = "courses";
    final String STUDENT_REF = "students";
    final String TEACHER_REF = "teachers";
    final String FEEDBACK_REF = "feedback";

    BaseManager(){

    }

    ChangeListener getListener(){
        return this.listener;
    }

    //Sets the event listener
    public void setChangeListener(ChangeListener listener) {
        this.listener = listener;
    }

    /** Sets up Change listener for the items list */
    public interface ChangeListener {
        void onChangeHappened();
    }

    /** Helper method that returns a db reference to Firebase */
    DatabaseReference getRef(String ref){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        return database.getReference(ref);
    }

    /** Helper method that returns a db reference to Firebase */
    DatabaseReference getRef(String ref, String ref2){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        return database.getReference(ref + "/" + ref2);
    }

    static void log(String s){
        Log.i("mgw", s);
    }
}

