package com.gabelwright.teacheraid.managers;

import androidx.annotation.NonNull;

import com.gabelwright.teacheraid.Utils;
import com.gabelwright.teacheraid.items.Course;
import com.gabelwright.teacheraid.items.Item;
import com.gabelwright.teacheraid.managers.Manager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class CourseManager extends Manager {

    public CourseManager(String key){
        super(new ArrayList<Item>(), key);

        DatabaseReference ref = getRef(COURSE_REF, key);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                clearList();
                for(DataSnapshot data : dataSnapshot.getChildren()){
                    Course c = data.getValue(Course.class);
                    addItem(c);
                }
                sortByAlpha();
                if (getListener() != null) {
                    getListener().onChangeHappened();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void createCourse(String name) {
        DatabaseReference ref = getRef(COURSE_REF, getKey());
        String course_id = getKey() + Utils.genRandomKey();

        Course c = new Course(name, 0, course_id);
        ref.child(course_id).setValue(c);
    }

    public void deleteCourse(int position){
        Course course = getCourse(position);
        DatabaseReference ref = getRef(COURSE_REF, Utils.parseTeacherId(course.getKey()));
        ref.child(course.getKey()).removeValue();
    }

    public void updateName(int position, String name){
        Course course = getCourse(position);
        DatabaseReference ref = getRef(COURSE_REF, Utils.parseTeacherId(course.getKey()));
        ref.child(course.getKey()).child("name").setValue(name);
    }

    public Course getCourse(int i){
        return (Course) getItem(i);
    }

}
