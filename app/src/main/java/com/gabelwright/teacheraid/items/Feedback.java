package com.gabelwright.teacheraid.items;

import java.util.Date;

public class Feedback extends Item {
    private boolean isPositive;

    public Feedback(String _name, String key, boolean isPositive) {
        super(_name, new Date().getTime(), key);
        this.isPositive = isPositive;
    }

    public Feedback(){
        this("", "", true);
    }

    public boolean isPositive() {
        return isPositive;
    }

    public void setPositive(boolean positive) {
        isPositive = positive;
    }
}
