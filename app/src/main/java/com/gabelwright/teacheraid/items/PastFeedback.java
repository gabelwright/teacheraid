package com.gabelwright.teacheraid.items;

import java.util.ArrayList;
import java.util.List;

public class PastFeedback extends Item {
    private List<Feedback> feedbackList;

    public PastFeedback(String _name, long _created, String key) {
        super(_name, _created, key);
        feedbackList = new ArrayList<>();
    }

    public void addFeedback(Feedback feedback){
        feedbackList.add(feedback);
    }

    public int feedbackCount(){
        return feedbackList.size();
    }
}
