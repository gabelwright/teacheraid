package com.gabelwright.teacheraid.items;

import java.util.Date;

public class Chat extends Item {

    public Chat(String name, String key){
        super(name, new Date().getTime(), key);
    }

    public Chat(){
        this("", "");
    }
}
