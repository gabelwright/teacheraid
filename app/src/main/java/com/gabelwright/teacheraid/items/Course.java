package com.gabelwright.teacheraid.items;

import java.util.Date;

public class Course extends Item {
    private int size;

    public Course(String name, int size, String key){
        super(name, new Date().getTime(), key);
        this.size = size;
    }

    public Course(){
        this("", 0, "");
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
