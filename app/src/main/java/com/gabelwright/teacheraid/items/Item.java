package com.gabelwright.teacheraid.items;

import android.util.Log;

import java.io.Serializable;
import java.util.Comparator;

public abstract class Item implements Serializable {
    private String name;
    private long created;
    private String key;

    public Item(String _name, long _created, String key){
        this.name = _name;
        this.created = _created;
        this.key = key;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String toString(){
        return name;
    }

    static void log(String s){
        Log.i("mgw", s);
    }

}

