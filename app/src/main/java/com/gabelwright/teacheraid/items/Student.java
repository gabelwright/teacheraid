package com.gabelwright.teacheraid.items;

import androidx.annotation.NonNull;

import com.gabelwright.teacheraid.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;

public class Student extends Item {
    private long updated;

    public Student(String name, String key, long updated){
        super(name, new Date().getTime(), key);
        this.updated = updated;
    }

    public Student(String key){
        this("", "", 0);
        setKey(key);

        String course_id = Utils.parseCourseId(key);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("students/" + course_id + "/" + key);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot != null){
                    setUpdated((long)dataSnapshot.child("updated").getValue());
                    setName((String)dataSnapshot.child("name").getValue());
                    setCreated((long)dataSnapshot.child("created").getValue());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public Student(){
        this("", "", 0);
    }

    public long getUpdated() {
        return updated;
    }

    public void setUpdated(long updated) {
        this.updated = updated;
    }

    public String updatedAsString(){
        if(updated == 0)
            return "No Feedback";
        return Utils.convertToDate(updated);
    }

    public void updateFromKey(String key){
        setKey(key);

        String course_id = Utils.parseCourseId(key);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("students/" + course_id + "/" + key);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                setUpdated((long)dataSnapshot.child("updated").getValue());
                setName((String)dataSnapshot.child("name").getValue());
                setCreated((long)dataSnapshot.child("created").getValue());
                log(getName());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
