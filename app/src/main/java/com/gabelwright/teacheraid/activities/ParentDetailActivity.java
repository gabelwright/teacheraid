package com.gabelwright.teacheraid.activities;

import android.os.Bundle;
import android.view.MenuItem;

import com.gabelwright.teacheraid.R;
import com.gabelwright.teacheraid.Utils;
import com.gabelwright.teacheraid.fragments.ChatFragment;
import com.gabelwright.teacheraid.fragments.LeaveFeedbackFragment;
import com.gabelwright.teacheraid.fragments.PastFeedbackFragment;
import com.gabelwright.teacheraid.items.Student;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class ParentDetailActivity extends DetailBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_detail);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(R.id.parent_feedback, R.id.parent_chat).build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                ActionBar actionBar = getSupportActionBar();
                if(menuItem.getItemId() == R.id.parent_feedback){
                    setFragment(new PastFeedbackFragment());
                    actionBar.setTitle("Past Feedback");
                }
                else if(menuItem.getItemId() == R.id.parent_chat){
                    setFragment(new ChatFragment());
                    actionBar.setTitle("Chat");
                }

                return true;
            }
        });
    }

//    private void setFragment(Fragment fragment){
//        for (Fragment oldFragment : getSupportFragmentManager().getFragments()) {
//            getSupportFragmentManager().beginTransaction().remove(oldFragment).commit();
//        }
//
//        FragmentManager manager = getSupportFragmentManager();
//        FragmentTransaction transaction = manager.beginTransaction();
//
//        transaction.replace(R.id.nav_host_fragment, fragment);
//        transaction.commit();
//    }

//    public String getStudentId(){
//        return Utils.getUserId(getApplicationContext(), this.getString(R.string.PARENT_ID));
//    }


}
