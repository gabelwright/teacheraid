package com.gabelwright.teacheraid.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.gabelwright.teacheraid.R;
import com.gabelwright.teacheraid.Utils;
import com.gabelwright.teacheraid.items.Course;
import com.gabelwright.teacheraid.items.Item;
import com.gabelwright.teacheraid.managers.CourseManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.InputType;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

public class ClassListActivity extends AppCompatActivity implements CourseManager.ChangeListener{

    List<Item> courses;
    CourseListAdaptor courseListAdaptor;
    CourseManager courseManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String teacher_id = Utils.getUserId(this, getResources().getString(R.string.TEACHER_ID));

        courseManager = new CourseManager(teacher_id);
        courseManager.setChangeListener(this);
        courses = courseManager.getItems();

        ListView listView = findViewById(R.id.class_list_view);
        courseListAdaptor = new CourseListAdaptor(this, courses);
        listView.setAdapter(courseListAdaptor);
        registerForContextMenu(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), StudentListActivity.class);
                Course course = courseManager.getCourse(i);
                intent.putExtra("course", (Serializable) course);
                startActivity(intent);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = buildDialog(-1);
                builder.show();
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.class_list_popup_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int id = item.getItemId();
        final int position = (int)info.id;

        if(id == R.id.class_list_popup_delete){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Delete");
            builder.setMessage("Are you sure you want to delete " + courseManager.getCourse(position).getName() + "?");
            builder.setIcon(android.R.drawable.ic_dialog_alert);
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    courseManager.deleteCourse(position);
                }
            });
            builder.setNegativeButton(android.R.string.no, null);
            builder.show();
        }
        else if(id == R.id.class_list_popup_edit){
            AlertDialog.Builder builder = buildDialog(position);
            builder.show();
        }

        return true;
    }

    public AlertDialog.Builder buildDialog(final int position){
        Course course;
        if (position != -1)
            course = courseManager.getCourse(position);
        else
            course = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(ClassListActivity.this);
        builder.setTitle("What is the name of the course?");

        final EditText input = new EditText(ClassListActivity.this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        if(position != -1)
            input.setText(course.getName());
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String m_Text = input.getText().toString();
                if(position == -1)
                    courseManager.createCourse(m_Text);
                else
                    courseManager.updateName(position, m_Text);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        return builder;
    }

    @Override
    public void onChangeHappened() {
        courseListAdaptor.notifyDataSetChanged();
    }

    private static class CourseListAdaptor extends ArrayAdapter<Item> {

        private final List<Item> courses;

        CourseListAdaptor(Context context, List<Item> courses){
            super(context, -1, courses);
            this.courses = courses;
        }

        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent){
            View row = convertView != null ? convertView
                    : LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.class_list_row, parent, false);

            TextView className = row.findViewById(R.id.class_list_row_name);
            TextView size = row.findViewById(R.id.class_list_row_size);

            Course course = (Course) courses.get(position);
            className.setText(course.getName());
            size.setText(course.getSize() + "");

            return row;
        }
    }

    private static void log(String s){
        Log.i("mgw", s);
    }

}


