package com.gabelwright.teacheraid.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gabelwright.teacheraid.R;
import com.gabelwright.teacheraid.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class EnterCodeActivity extends AppCompatActivity {

    ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_code);

        spinner = findViewById(R.id.enter_code_spinner);
        spinner.setVisibility(View.INVISIBLE);
    }

    public void submitCode(View view){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(EnterCodeActivity.this);
        alertDialog.setTitle("Enter the code provided to you.");

        final EditText input = new EditText(EnterCodeActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(convertToPixels(30), 0, convertToPixels(30), 0);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                spinner.setVisibility(View.VISIBLE);
                saveCode(input.getText().toString());
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
                saveCode(contents);
            }
            if(resultCode == RESULT_CANCELED){
                spinner.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "There was an error, please try again.", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void saveCode(final String code){
        log("code entered");
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("students/" + Utils.parseCourseId(code) +  "/" + code);
        log("ref found: " + ref.toString());

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() == null){
                    Toast.makeText(getApplicationContext(), "Student not found, please check your code and try again.", Toast.LENGTH_LONG).show();
                    spinner.setVisibility(View.INVISIBLE);
                }
                else{
                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(getApplicationContext().getString(R.string.USER_TYPE_KEY), getApplicationContext().getString(R.string.PARENT_KEY));
                    editor.putString(getApplicationContext().getString(R.string.PARENT_ID), code);
                    editor.commit();
                    spinner.setVisibility(View.INVISIBLE);

                    Intent intent = new Intent(getApplicationContext(), ParentDetailActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                log("canceled");
            }
        });

    }

    public void scanCode(View view){
        spinner.setVisibility(View.VISIBLE);
        try {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");

            startActivityForResult(intent, 0);

        } catch (Exception e) {
            spinner.setVisibility(View.VISIBLE);
            Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
            Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
            startActivity(marketIntent);
        }
    }

    private int convertToPixels(int dp){
        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (dp * scale + 0.5f);
        return dpAsPixels;
    }

    private static void log(String s){
        Log.i("mgw", s);
    }
}
