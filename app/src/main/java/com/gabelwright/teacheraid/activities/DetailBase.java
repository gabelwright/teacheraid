package com.gabelwright.teacheraid.activities;

import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.gabelwright.teacheraid.R;
import com.gabelwright.teacheraid.Utils;
import com.gabelwright.teacheraid.fragments.ChatFragment;
import com.gabelwright.teacheraid.items.Student;

public class DetailBase extends AppCompatActivity {

    void setFragment(Fragment fragment){
        for (Fragment oldFragment : getSupportFragmentManager().getFragments()) {
            getSupportFragmentManager().beginTransaction().remove(oldFragment).commit();
        }

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        transaction.replace(R.id.nav_host_fragment, fragment);
        transaction.commit();
    }

    public String getStudentId(){
        return Utils.getUserId(getApplicationContext(), this.getString(R.string.PARENT_ID));
    }

    public void sendMessage(View view){
        EditText message = findViewById(R.id.chat_tab_edit_text);

        ChatFragment fragment = (ChatFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        String user_type = Utils.getUserType(getApplicationContext());
        fragment.sendMessage(message.getText().toString(), user_type);
        message.setText("");
    }

    static void log(String s){
        Log.i("mgw", s);
    }
}
