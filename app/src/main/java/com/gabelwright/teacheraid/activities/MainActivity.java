package com.gabelwright.teacheraid.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.gabelwright.teacheraid.R;
import com.gabelwright.teacheraid.Utils;
import com.gabelwright.teacheraid.items.Student;
import com.gabelwright.teacheraid.managers.TeacherManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkSharedPref();
    }

    private void checkSharedPref(){
        log("Checking prefs");
        String user_type = Utils.getUserType(this);
        if(user_type.equals(this.getString(R.string.TEACHER_KEY))){
            log("teacher type found");
            Intent intent = new Intent(this, ClassListActivity.class);
            startActivity(intent);
        }
        else if(user_type.equals(this.getString(R.string.PARENT_KEY))){
            log("parent type found");
//            Student student = new Student(Utils.getUserId(getApplicationContext(), this.getString(R.string.PARENT_ID)));
//            student.updateFromKey(Utils.getUserId(getApplicationContext(), this.getString(R.string.PARENT_ID)));

            Intent intent = new Intent(this, ParentDetailActivity.class);
//            intent.putExtra("student", student);
            startActivity(intent);
        }
        else{
            log(user_type);
        }
    }

    private void createTeacher(String name){
        TeacherManager teacherManager = new TeacherManager();
        String key = teacherManager.createTeacher(name);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(this.getString(R.string.USER_TYPE_KEY), this.getString(R.string.TEACHER_KEY));
        editor.putString(this.getString(R.string.TEACHER_ID), key);
        editor.commit();

        Intent intent = new Intent(this, ClassListActivity.class);
        startActivity(intent);
    }

    public void createParent(View view){
//        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
//        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.putString(this.getString(R.string.USER_TYPE_KEY), this.getString(R.string.TEACHER_KEY));
//        editor.putString(this.getString(R.string.TEACHER_ID), "jhkjhkj");
//        editor.commit();

        Intent intent = new Intent(this, EnterCodeActivity.class);
        startActivity(intent);
    }

    public void askForName(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("What is your name?");

        final EditText input = new EditText(MainActivity.this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = input.getText().toString();
                createTeacher(name);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private static void log(String s){
        Log.i("mgw", s);
    }
}
