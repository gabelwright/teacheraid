package com.gabelwright.teacheraid.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.gabelwright.teacheraid.R;
import com.gabelwright.teacheraid.fragments.ChatFragment;
import com.gabelwright.teacheraid.fragments.LeaveFeedbackFragment;
import com.gabelwright.teacheraid.fragments.PastFeedbackFragment;
import com.gabelwright.teacheraid.fragments.ShareFragment;
import com.gabelwright.teacheraid.items.Student;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class StudentDetailActivity extends DetailBase{

    Student student;
    Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_detail);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(R.id.leave_feedback, R.id.past_feedback, R.id.chat, R.id.share).build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        student = (Student) getIntent().getSerializableExtra("student");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(student.getName() + " - " + getString(R.string.tab_leave_feedback));

        setFragment(new LeaveFeedbackFragment());

        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                ActionBar actionBar = getSupportActionBar();
                if(menuItem.getItemId() == R.id.leave_feedback){
                    setFragment(new LeaveFeedbackFragment());
                    actionBar.setTitle(student.getName() + " - " + getString(R.string.tab_leave_feedback));
                    showOption(R.id.leave_feedback_menu_save);
                }
                else if(menuItem.getItemId() == R.id.past_feedback){
                    setFragment(new PastFeedbackFragment());
                    actionBar.setTitle(student.getName() + " - " + getString(R.string.tab_past_feedback));
                    hideOption(R.id.leave_feedback_menu_save);
                }
                else if(menuItem.getItemId() == R.id.chat){
                    setFragment(new ChatFragment());
                    actionBar.setTitle(student.getName() + " - " + getString(R.string.tab_chat));
                    hideOption(R.id.leave_feedback_menu_save);
                }
                else if(menuItem.getItemId() == R.id.share){
                    setFragment(new ShareFragment());
                    actionBar.setTitle(student.getName() + " - " + getString(R.string.share));
                    hideOption(R.id.leave_feedback_menu_save);
                }

                return true;
            }
        });

    }

//    private void setFragment(Fragment fragment){
//        for (Fragment oldFragment : getSupportFragmentManager().getFragments()) {
//            getSupportFragmentManager().beginTransaction().remove(oldFragment).commit();
//        }
//
//        FragmentManager manager = getSupportFragmentManager();
//        FragmentTransaction transaction = manager.beginTransaction();
//
//        transaction.replace(R.id.nav_host_fragment, fragment);
//        transaction.commit();
//    }

    private void hideOption(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(false);
    }

    private void showOption(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(true);
    }

    @Override
    public String getStudentId(){
        return student.getKey();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.leave_feedback_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.leave_feedback_menu_save){
            LeaveFeedbackFragment fragment = (LeaveFeedbackFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
            fragment.saveFeedback();
        }
        else if(item.getItemId() == getResources().getInteger(R.integer.student_detail_back_button)){
            onBackPressed();
        }
        return true;
    }

}
