package com.gabelwright.teacheraid.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.gabelwright.teacheraid.R;
import com.gabelwright.teacheraid.items.Course;
import com.gabelwright.teacheraid.items.Item;
import com.gabelwright.teacheraid.items.Student;
import com.gabelwright.teacheraid.managers.StudentManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.InputType;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class StudentListActivity extends AppCompatActivity implements StudentManager.ChangeListener{

    StudentManager studentManager;
    Course course;
    StudentListAdaptor studentListAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        course = (Course) intent.getSerializableExtra("course");
        assert course != null;
        setTitle(course.getName());

        studentManager = new StudentManager(course.getKey());
        studentManager.setChangeListener(this);

        final ListView listView = findViewById(R.id.student_list_view);
        studentListAdaptor = new StudentListAdaptor(this, studentManager.getItems());
        listView.setAdapter(studentListAdaptor);
        registerForContextMenu(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent new_activity = new Intent(getApplicationContext(), StudentDetailActivity.class);
                new_activity.putExtra("student", studentListAdaptor.getItem(position));
                startActivity(new_activity);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = buildDialog(-1, "");
                builder.show();
            }
        });
    }

    /**
     * Creates a popup menu when an item is long-pressed
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.class_list_popup_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int id = item.getItemId();
        final int position = (int)info.id;

        if(id == R.id.class_list_popup_delete){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Delete");
            builder.setMessage("Are you sure you want to delete " + studentManager.getStudent(position).getName() + "?");
            builder.setIcon(android.R.drawable.ic_dialog_alert);
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    studentManager.deleteStudent(position);
                }
            });
            builder.setNegativeButton(android.R.string.no, null);
            builder.show();
        }else if(id == R.id.class_list_popup_edit){
            AlertDialog.Builder builder = buildDialog(position, studentManager.getStudent(position).getName());
            builder.show();
        }

        return true;
    }

    public AlertDialog.Builder buildDialog(final int position, String name){
        AlertDialog.Builder builder = new AlertDialog.Builder(StudentListActivity.this);
        builder.setTitle("What is the name of the student?");

        final EditText input = new EditText(StudentListActivity.this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(name);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String m_Text = input.getText().toString();
                if(position == -1)
                    studentManager.createStudent(m_Text);
                else
                    studentManager.updateName(position, m_Text);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        return builder;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onChangeHappened() {
        studentListAdaptor.notifyDataSetChanged();
    }

    private static class StudentListAdaptor extends ArrayAdapter<Item> {

        private final List<Item> students;

        StudentListAdaptor(Context context, List<Item> students){
            super(context, -1, students);
            this.students = students;
        }

        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent){
            View row = convertView != null ? convertView
                    : LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.student_list_row, parent, false);

            TextView studentName = row.findViewById(R.id.student_list_name);
            TextView updated = row.findViewById(R.id.student_list_last_feedback);

            Student student = (Student) students.get(position);
            studentName.setText(student.getName());
            updated.setText(student.updatedAsString());

            return row;
        }
    }

    private static void log(String s){
        Log.i("mgw", s);
    }

}
