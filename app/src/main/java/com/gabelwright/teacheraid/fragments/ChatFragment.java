package com.gabelwright.teacheraid.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.gabelwright.teacheraid.R;
import com.gabelwright.teacheraid.activities.DetailBase;
import com.gabelwright.teacheraid.activities.StudentDetailActivity;
import com.gabelwright.teacheraid.Utils;
import com.gabelwright.teacheraid.items.Chat;
import com.gabelwright.teacheraid.managers.ChatManager;
import com.gabelwright.teacheraid.items.Item;

import java.util.List;

public class ChatFragment extends Fragment implements ChatManager.ChangeListener{

    ChatManager chatManager;
    ListView listView;
    ChatListAdaptor chatListAdaptor;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_chat, container, false);

        chatManager = new ChatManager(((DetailBase)getActivity()).getStudentId());
        chatManager.setChangeListener(ChatFragment.this);
        listView = root.findViewById(R.id.chat_tab_list_view);

        String user_type = Utils.getUserType(getContext());

        chatListAdaptor = new ChatListAdaptor(getContext(), chatManager.getItems(), user_type);

        listView.setAdapter(chatListAdaptor);

        return root;
    }

    public void sendMessage(String message, String key){
        chatManager.saveMessage(message, key);
    }

    @Override
    public void onChangeHappened() {
        chatListAdaptor.notifyDataSetChanged();
        listView.setSelection(chatListAdaptor.getCount() - 1);
    }

    private static class ChatListAdaptor extends ArrayAdapter<Item> {

        private final List<Item> chats;
        private String user_type;

        ChatListAdaptor(Context context, List<Item> chats, String user_type){
            super(context, -1, chats);
            this.chats = chats;
            this.user_type = user_type;
        }

        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent){
            View row;
            Chat chat = (Chat) chats.get(position);

            if(chat.getKey().equals(user_type)){
                row = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_row_yours, parent, false);
            }else{
                row = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_row_theirs, parent, false);
            }

            TextView message = row.findViewById(R.id.chat_row_yours_message);
            message.setText(chat.getName());

            return row;
        }
    }

    private static void log(String s){
        Log.i("mgw", s);
    }

}