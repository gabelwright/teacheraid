package com.gabelwright.teacheraid.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.gabelwright.teacheraid.PastFeedbackListAdaptor;
import com.gabelwright.teacheraid.R;
import com.gabelwright.teacheraid.Utils;
import com.gabelwright.teacheraid.activities.DetailBase;
import com.gabelwright.teacheraid.activities.StudentDetailActivity;
import com.gabelwright.teacheraid.items.PastFeedback;
import com.gabelwright.teacheraid.managers.PastFeedbackManager;

public class PastFeedbackFragment extends Fragment implements PastFeedbackManager.ChangeListener{

    PastFeedbackManager pastFeedbackManager;
    PastFeedbackListAdaptor pastFeedbackListAdaptor;
    ExpandableListView listView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_past_feedback, container, false);

        pastFeedbackManager = new PastFeedbackManager(((DetailBase)getActivity()).getStudentId());
        pastFeedbackManager.setChangeListener(PastFeedbackFragment.this);
        pastFeedbackListAdaptor = new PastFeedbackListAdaptor(getContext(), pastFeedbackManager.getHeaders(), pastFeedbackManager.getChildFeedback());
        listView = root.findViewById(R.id.past_feedback_list_view);
        listView.setAdapter(pastFeedbackListAdaptor);

        registerForContextMenu(listView);

        return root;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        String user_type = Utils.getUserType(getContext());
        if(user_type.equals(getContext().getResources().getString(R.string.TEACHER_KEY))){
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.past_feedback_popup, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item.getMenuInfo();
        int id = item.getItemId();
        final int position = ExpandableListView.getPackedPositionGroup(info.packedPosition);

        if(id == R.id.past_feedback_popup_delete){
            if(pastFeedbackManager.isTodaysFeedback(position)){
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Delete");
                builder.setMessage("Are you sure you want to delete " + pastFeedbackManager.getHeaderName(position) + "?");
                builder.setIcon(android.R.drawable.ic_dialog_alert);
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        pastFeedbackManager.deleteGroup(position);
                    }
                });
                builder.setNegativeButton(android.R.string.no, null);
                builder.show();
            }
            else{
                Toast.makeText(getContext(), "Only today's feedback can be deleted. Past feedback cannot be altered.", Toast.LENGTH_LONG).show();
            }

        }

        return true;
    }

    @Override
    public void onChangeHappened() {
        pastFeedbackListAdaptor.notifyDataSetChanged();
    }

    private static void log(String s){
        Log.i("mgw", s);
    }
}