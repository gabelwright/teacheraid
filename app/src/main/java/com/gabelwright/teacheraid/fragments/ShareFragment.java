package com.gabelwright.teacheraid.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gabelwright.teacheraid.R;
import com.gabelwright.teacheraid.activities.StudentDetailActivity;
import com.google.zxing.WriterException;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;


public class ShareFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_share, container, false);

        TextView code = root.findViewById(R.id.qr_code_text);
        final String student_id = ((StudentDetailActivity)getActivity()).getStudentId();
        code.setText(student_id);

        QRGEncoder qrgEncoder = new QRGEncoder(student_id, null, QRGContents.Type.TEXT, 1028);

        try {
            // Getting QR-Code as Bitmap
            Bitmap bitmap = qrgEncoder.encodeAsBitmap();
            // Setting Bitmap to ImageView
            ImageView qrImage = root.findViewById(R.id.share_qr_image);
            qrImage.setImageBitmap(bitmap);
        } catch (WriterException e) {
            Log.v("mgw", e.toString());
        }

        code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("student_id", student_id);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getContext(), "Code copied to clipboard.", Toast.LENGTH_LONG).show();
            }
        });






        return root;
    }

    private static void log(String s){
        Log.i("mgw", s);
    }
}
