package com.gabelwright.teacheraid.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.gabelwright.teacheraid.items.Feedback;
import com.gabelwright.teacheraid.managers.FeedbackManager;
import com.gabelwright.teacheraid.items.Item;
import com.gabelwright.teacheraid.R;
import com.gabelwright.teacheraid.activities.StudentDetailActivity;

import java.util.List;

public class LeaveFeedbackFragment extends Fragment {

    private FeedbackManager feedbackManager;
    private ListView listView;
    private FeedbackListAdaptor feedbackListAdaptor;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_leave_feedback, container, false);

        feedbackManager = new FeedbackManager("");
        feedbackManager.populateWithNewFeedback();
        List<Item> feedbackList = feedbackManager.getItems();

        listView = root.findViewById(R.id.leave_feedback_list_view);

        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setItemsCanFocus(false);

        feedbackListAdaptor = new FeedbackListAdaptor(getContext(), feedbackList);
        listView.setAdapter(feedbackListAdaptor);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SparseBooleanArray sbArray = listView.getCheckedItemPositions();
                Feedback feedback = (Feedback)feedbackListAdaptor.getItem(i);
                CardView card = getCardFromView(view);
                assert feedback != null;
                card.setCardBackgroundColor(getBackgroundColor(feedback, sbArray.get(i)));
            }
        });

        return root;
    }

    public void saveFeedback(){
        String studentId = ((StudentDetailActivity)getActivity()).getStudentId();
        SparseBooleanArray sbArray = listView.getCheckedItemPositions();
        feedbackManager.leaveFeedback(sbArray, studentId);
        Toast.makeText(getContext(), "Feedback saved", Toast.LENGTH_LONG).show();
        resetFeedbackList();
        getActivity().onBackPressed();
    }

    private CardView getCardFromView(View view){
        ViewGroup viewGroup = (ViewGroup) view;
        return (CardView)viewGroup.getChildAt(0);
    }

    private int getBackgroundColor(Feedback feedback, boolean isSelected){
        int color;
        if(feedback.isPositive())
            if(isSelected) {
                color = R.color.positive_feedback_selected;
            }
            else {
                color = R.color.positive_feedback;
            }
        else
            if(isSelected) {
                color = R.color.negative_feedback_selected;
            }
            else {
                color = R.color.negative_feedback;
            }
        return getContext().getResources().getColor(color);
    }

    private void resetFeedbackList(){
        feedbackListAdaptor.notifyDataSetChanged();
        listView.clearChoices();
    }

    /** Converts dp units to px units */
    private int dpToPx(int dp){
        Resources r = getContext().getResources();

        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    private static class FeedbackListAdaptor extends ArrayAdapter<Item> {

        private final List<Item> feedbackList;

        FeedbackListAdaptor(Context context, List<Item> feedbackList){
            super(context, -1, feedbackList);
            this.feedbackList = feedbackList;
        }

        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent){
            View row = convertView != null ? convertView
                    : LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.feedback_list_row, parent, false);

            TextView text = row.findViewById(R.id.feedback_row_text);
            CardView card = row.findViewById(R.id.feedback_row_card);

            Feedback feedback = (Feedback) feedbackList.get(position);

            text.setText(feedback.getName());
            if(feedback.isPositive())
                card.setCardBackgroundColor(getContext().getResources().getColor(R.color.positive_feedback));
            else
                card.setCardBackgroundColor(getContext().getResources().getColor(R.color.negative_feedback));


            return row;
        }
    }

    private static void log(String s){
        Log.i("mgw", s);
    }
}